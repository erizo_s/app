package by.boiko.crm.service.impl;


import by.boiko.crm.dao.UserDao;
import by.boiko.crm.model.User;
import by.boiko.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public List<User> getAll() {
        return userDao.loadAll();
    }

    public User getAllForId(int id) {
        return userDao.loadAllUsersForId(id);
    }

    public void delete(int id) {
        userDao.delete(id);
    }

    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }
}