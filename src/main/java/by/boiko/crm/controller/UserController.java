package by.boiko.crm.controller;


import by.boiko.crm.model.User;
import by.boiko.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * The controller determines methods for access to User service.
 */

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Returns list of all users.
     *
     * @return list of users
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getAllUsers() {
        ModelAndView mv = new ModelAndView("list");
        mv.addObject("users", userService.getAll());
        return userService.getAll();
    }


    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public User getStudent(@PathVariable("id") int id, @RequestBody User user) {
        user.setId(id);
        userService.update(user);
        return userService.getAllForId(id);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getStudent(@PathVariable("id") int id) {

        userService.getAllForId(id);
        return userService.getAllForId(id);
    }

    /**
     * Deletes a user by identifier.
     *
     * @param userId identifier of a user to delete
     * @return refresh the page
     */
    @RequestMapping(value = "/user/{userId}", method = RequestMethod.DELETE)
    @ResponseBody
    public List<User> deleteUser(@PathVariable("userId") int userId) {
        userService.delete(userId);
        return userService.getAll();
    }

    /**
     * Save a new user.
     *
     * @return to page with all users
     */
    @RequestMapping(value = "/user/new", method = RequestMethod.POST)
    @ResponseBody
    public List<User> saveUser(@RequestBody User user) {
        userService.save(user);
        return userService.getAll();
    }

}
